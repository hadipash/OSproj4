#ifndef BOUNDEDBUFFER_H
#define BOUNDEDBUFFER_H

#include "synch.h"

class BoundedBuffer {
  public:
    BoundedBuffer(int maxsize); // Create a bounded buffer to hold at most maxsize bytes
    ~BoundedBuffer();		        // De-allocate something

    int Read(void* data, int size); // Read size bytes from the buffer, blocking as necessary until
                                    // enough bytes are available to completely satisfy the request.
                                    // Copy the bytes into memory starting at address data.
                                    // Return the number of bytes successfully read.

    int Write(void* data, int size);  // Write size bytes into the buffer, blocking as necessary until
                                      // enough space is available to completely satisfy the request.
                                      // Copy the bytes from memory starting at address data.
                                      // Return the number of bytes successfully written.

    void Close();   // Permanently close the BoundedBuffer object. This releases any
                    // blocked readers or writers; any subsequent attempts to Read
                    // or Write the buffer fail and return 0.

  private:
    void* buffer;       // Bounded buffer
    int bufSize;        // Size of the buffer
    int in, out;        // Pointers to empty space and the first element in the buffer
    bool closed;        // Whether the buffer is closed or not

    Semaphore *mutex;   // Enforce mutual exclusive access to the buffer
    Semaphore *full;    // Writers wait if the buffer is full
    Semaphore *empty;   // Readers wait if the buffer is empty
};

#endif // BOUNDEDBUFFER_H
