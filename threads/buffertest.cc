#include "buffertest.h"

void Producer(BoundedBuffer *testBuffer) {
    int writenBytes;
    char* testString = "BufferTest";
    writenBytes = testBuffer->Write(testString, 10);
    printf("%s wrote %i bytes\n", kernel->currentThread->getName(), writenBytes);

}

void Consumer(BoundedBuffer *testBuffer) {
    int readBytes;
    char* testString = new char[10];
    readBytes = testBuffer->Read(testString, 10);
    printf("%s read %i bytes\n", kernel->currentThread->getName(), readBytes);
}

void BufferTest()
{
    int bufferSize;

    printf("Buffer size: ");
    scanf("%i", &bufferSize);
    BoundedBuffer *testBuffer = new BoundedBuffer(bufferSize);

    Thread *t1 = new Thread("Thread1");
    Thread *t2 = new Thread("Thread2");
    Thread *t3 = new Thread("Thread3");
    Thread *t4 = new Thread("Thread4");

    t1->Fork((VoidFunctionPtr) Producer, testBuffer);
    t2->Fork((VoidFunctionPtr) Consumer, testBuffer);
    t3->Fork((VoidFunctionPtr) Producer, testBuffer);
    t4->Fork((VoidFunctionPtr) Consumer, testBuffer);

    kernel->alarm->WaitUntil(1000000); // let producers and consumers work
    delete testBuffer;
}
