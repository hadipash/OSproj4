#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <ctime>
#include "main.h"

using namespace std;
int *threadArray;	// array of threads
									// defined as a global variable to protect from deletion

void generate(DLList *list, int n, int which) {
	// generate N items and insert them into the list
	for (int i = 0; i < n; i++) {
		void *item = NULL;
		item = (void*)(rand() % 100);
		list->SortedInsert(item, (int)item);
		cout << "Item " << (int)item << " has been inserted into the list by Thread#" << which << endl;
	}
}

void remove(DLList *list, int n, int which) {
	// remove N elements or until the list has no more elements
	for (int i = 0; i < n && !list->IsEmpty(); i++) {
		void *item = NULL;
		int *key = new int;

		item = list->Remove(key);
		if(key)
			cout << "Item " << *key << " has been removed from the list by Thread#" << which << endl;

		delete key;
	}
}

// fucntion in which each thread generates and removes elements from the list
void SimpleTask(void **args) {
	int which = *((int *)args[0]);
	int N = *((int *)args[1]);
	DLList *list = (DLList *)args[2];

	generate(list, N, which);
	cout << endl;

	kernel->currentThread->Yield();

	remove(list, N, which);
	cout << endl;

	free(args);
}

void HW1Test(int T, int N) {
	DLList *list = new DLList;
	threadArray = new int [T];
	srand(time(0));
	cout << endl;

	// create T-1 child threads
	for(int i = 1; i < T; i++) {
		Thread *t = new Thread("forked thread");
		threadArray[i] = i;

		void **args;
		args = (void **)malloc(3 * sizeof(void *));
		args[0] = (void *)&threadArray[i];
		args[1] = (void *)&N;
		args[2] = list;

		t->Fork((VoidFunctionPtr) SimpleTask, args);
	}

	threadArray[0] = 0;
	void **args;
	args = (void **)malloc(3 * sizeof(void *));
	args[0] = (void *)&threadArray[0];
	args[1] = (void *)&N;
	args[2] = list;
	SimpleTask(args);

	delete(threadArray);
}
