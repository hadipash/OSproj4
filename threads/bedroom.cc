#include "bedroom.h"
#include "main.h"

BedRoom::BedRoom() {
  beds = new List<Bed>;
  current_interrupt = 0;
}

BedRoom::~BedRoom() {
  delete beds;
}

// put thread on the waiting queue
void BedRoom::PutToBed(Thread *t, int x) {
  DEBUG(dbgSleep, "Thread " << t << " will wake up at interrupt " << current_interrupt + x);
  // current_interrupt + x - time to wake a thread
  beds->Append(Bed(t, current_interrupt + x));
  //put thread to sleep, but do not finish it
  t->Sleep(false);
}

//find threads to wake up
bool BedRoom::MorningCall() {
  bool woken = false;
  // increase timer every morning call
  current_interrupt++;

  // iterator for the waiting queue
  ListIterator<Bed> *iter = new ListIterator<Bed>(beds);

  // find threads to wake
  for(;!iter->IsDone(); iter->Next()) {
      if(current_interrupt >= iter->Item().wakeTime) {
          woken = true;
          DEBUG(dbgSleep, "Thread " << iter->Item().sleeper << " woken");
          kernel->scheduler->ReadyToRun(iter->Item().sleeper);
          beds->Remove(iter->Item());
      }
  }

  delete iter;
  return woken;
}
