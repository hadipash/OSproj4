#include "boundedbuffer.h"
#include "stdio.h"

BoundedBuffer::BoundedBuffer(int maxsize) {
    bufSize = maxsize;
    in = out = 0;
    closed = false;
    buffer = (void*)malloc(bufSize);

    mutex = new Semaphore("mutex", 1);
    full = new Semaphore("buffer full", 0);
    empty = new Semaphore("buffer empty", bufSize);
}

BoundedBuffer::~BoundedBuffer() {
    Close();
}

int BoundedBuffer::Read(void* data, int size) {
    if(closed)
      return 0;

    int numOfBytes;     // the number of bytes successfully read

    for(numOfBytes = 0; numOfBytes < size; numOfBytes++){
      full->P();
      mutex->P();

      // if the buffer was closed in the middle of reading
      if(closed){
        mutex->V();
        full->V();
        return numOfBytes;
      }

      *((char *)data + numOfBytes) = *((char *)buffer + out);
      printf("\t\t\t\t\t%s reads %c from position %i\n", kernel->currentThread->getName(), *((char *)buffer + out), out);
      out = (out + 1) % bufSize;

      mutex->V();
      empty->V();
    }

    return numOfBytes;
}

int BoundedBuffer::Write(void* data, int size) {
    if(closed)
      return 0;

    int numOfBytes;     // the number of bytes successfully written

    for(numOfBytes = 0; numOfBytes < size; numOfBytes++){
      empty->P();
      mutex->P();

      // if the buffer was closed in the middle of writing
      if(closed){
        mutex->V();
        empty->V();
        return numOfBytes;
      }

      *((char *)buffer + in) = *((char *)data + numOfBytes);
      printf("\t%s writes %c on position %i\n", kernel->currentThread->getName(), *((char *)data + numOfBytes), in);
      in = (in + 1) % bufSize;

      mutex->V();
      full->V();
    }

    return numOfBytes;
}

void BoundedBuffer::Close() {
    closed = true;                  // close access to the buffer
    empty->V();                     // unblock writers
    full->V();                      // unblock readers

    kernel->currentThread->Yield(); // let current readers and writers finish
                                    // their tasks and be released

    free(buffer);                   // release system resources
    delete mutex;
    delete full;
    delete empty;
}
